
extern crate hyper;
extern crate serde_json;
extern crate dotenv;
extern crate clap;

use hyper::{Client, Url};
use std::io::Read;
use serde_json::{Value, Map};
use std::env;
use clap::{Arg, App, SubCommand};

struct HomeAssistant {
    hostname: String,
    port: u16,
    //api_password: String,
}

impl HomeAssistant {
    fn new(hostname: String, port: u16) -> HomeAssistant {
        HomeAssistant {
            hostname: hostname.to_string(),
            port: port,
            //api_password: "".to_string(),
        }
    }

    fn execute_service(&self, domain: &str, service: &str, entity_id: &str)
        -> hyper::Result<String>
    {
        let url = Url::parse(format!("http://{}:{}/api/services/{}/{}",
            self.hostname, self.port, domain, service).as_str())?;

        let mut data = Map::new();
        data.insert("entity_id".to_string(), Value::String(entity_id.to_string()));
        let body = serde_json::to_string(&data).unwrap();

        let client = Client::new();
        let mut response = client.post(url).body(&body).send()?;
        let mut buf = String::new();
        response.read_to_string(&mut buf)?;
        Ok(buf)
    }
}

fn main() {
    dotenv::dotenv().expect("Failed to load environment variables.");
    
    let hostname = env::var("HASS_HOST").unwrap();
    let port : u16 = env::var("HASS_PORT").unwrap().parse::<u16>().unwrap();
    let hass = HomeAssistant::new(hostname, port);

    // TODO: get these from Cargo.toml
    let matches = App::new("hasscli")
        .version("0.1.0")
        .author("Rob Connolly <rob@webworxshop.com>")
        .about("Home Assistant client written in Rust")
        .subcommand(SubCommand::with_name("execute_service")
            .about("Executes a Home Assistant service.")
            .arg(Arg::with_name("DOMAIN")
                 .required(true)
                 .index(1)
                 .help("Service domain"))
            .arg(Arg::with_name("SERVICE")
                 .required(true)
                 .index(2)
                 .help("Service name"))
            .arg(Arg::with_name("ENTITY")
                 .required(true)
                 .index(3)
                 .help("Entity ID")))
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("execute_service") {
        let domain = matches.value_of("DOMAIN").unwrap();
        let service = matches.value_of("SERVICE").unwrap();
        let entity_id = matches.value_of("ENTITY").unwrap();

        hass.execute_service(domain, service, entity_id).expect("An error occurred.");
    } else {
        panic!("No subcommand specified");
    }
}
