
# hasscli
## A basic Home Assistant client written in Rust

### About

This is my first real Rust program, I wrote it as an exercise in using the language
and some of it's available libraries. Right now it just executes HASS services from
the command line. Maybe I'll expand it in future.

### Building

```
cargo build --release
cargo install
```

This will install in `~/.cargo/bin` by default, make sure this is on your path.

### Usage

See `hasscli -h` for usage details.

### Licence

Apache 2.0 (same as Home Assistant), see
[LICENCE](https://gitlab.com/robconnolly/hasscli/blob/master/LICENSE).

